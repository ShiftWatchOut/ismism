worker_processes auto;
worker_rlimit_nofile 10000;

events {
    worker_connections 2048;
    multi_accept on;
}

http {
    access_log log/access.log combined buffer=128k flush=5m;
    error_log log/error.log error;

    client_header_timeout 10s;
    client_body_timeout 10s;
    send_timeout 10s;
    reset_timedout_connection on;

    gzip on;
    gzip_comp_level 4;
    gzip_min_length 1000;
    gzip_proxied any;
    gzip_types *;

    server_tokens off;
    charset utf-8;

    map $server_protocol $h1_addr {
        default "";
        "HTTP/1.0" $binary_remote_addr;
        "HTTP/1.1" $binary_remote_addr;
    }

    limit_conn_status 444;
    limit_conn_zone $binary_remote_addr zone=addr_conn:10m;
    limit_conn_zone $h1_addr zone=h1_conn:10m;
    limit_req_status 444;
    limit_req_zone $binary_remote_addr zone=addr_req:10m rate=100r/m;
    limit_req_zone $h1_addr zone=h1_req:10m rate=10r/m;

    limit_conn addr_conn 10;
    limit_req zone=addr_req burst=20 nodelay;
    limit_conn h1_conn 10;
    limit_req zone=h1_req burst=10 nodelay;

    error_page 400 403 404 500 502 503 504 =444 /444.html;

    types {
        text/html html;
        text/css css;
        application/javascript js;
        application/json json;
        application/pdf pdf;
        image/webp webp;
    }

    upstream ismism {
        server 127.0.0.1:728 fail_timeout=1h;
        server 127.0.0.1:729 backup;
        keepalive 2;
    }

    proxy_cache_path cache levels=1:2 keys_zone=cache:10m max_size=10g inactive=1d use_temp_path=off;

    server {
        server_name _;
        listen 80 default_server;
        listen [::]:80 default_server;
        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;

		http2 on;
        ssl_protocols TLSv1.3;
        ssl_certificate ssl/ismist.cn.crt;
        ssl_certificate_key ssl/ismist.cn.key;
        ssl_session_cache shared:SSL:50m;
        ssl_session_timeout 1d;
        ssl_session_tickets off;
        ssl_stapling on;
        ssl_stapling_verify on;

        location = /444.html {
            return 444;
        }

        return 444;
    }

    server {
        server_name ismist.cn;
        listen 80;
        listen [::]:80;

        location = /444.html {
            return 444;
        }

        location / {
            return 444;
        }

        location = / {
            limit_except GET {
                deny all;
            }
            return 301 https://$host;
        }
    }
    server {
        server_name ismist.cn localhost;
        listen 443 ssl;
        listen [::]:443 ssl;

		http2 on;
        ssl_protocols TLSv1.3;
        ssl_certificate ssl/ismist.cn.crt;
        ssl_certificate_key ssl/ismist.cn.key;
        ssl_session_cache shared:SSL:50m;
        ssl_session_timeout 1d;
        ssl_session_tickets off;
        ssl_stapling on;
        ssl_stapling_verify on;

        root ui;

        location = /444.html {
            return 444;
        }

        location / {
            return 444;
        }

        location = / {
            limit_except GET {
                deny all;
            }
            try_files /index.html =444;
        }

		location /mod {
			limit_except GET {
                deny all;
            }
		}

        proxy_buffer_size 128k;
        proxy_buffers 4 256k;
        proxy_busy_buffers_size 256k;
        proxy_cache cache;
        proxy_cache_methods GET;
        proxy_cache_valid any 1s;
        proxy_cache_revalidate on;
        proxy_cache_background_update on;
        proxy_cache_lock on;
        proxy_http_version 1.1;
        proxy_set_header Connection "";

        location /q {
            limit_except GET {
                deny all;
            }
            proxy_pass http://ismism;
        }

        location /p {
            limit_except POST {
                deny all;
            }
            proxy_pass http://ismism;
        }
    }
}
